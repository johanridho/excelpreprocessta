/*
    program buat misahin value atribut dalam satu cell yg dipisahkan dengan tanda koma
    cth diamond ada cell atribut color: grey,white -> bakal dipisah jadi dua data dengan masing2 color grey dan color white
    input: mineral.xlsx
    output mineral_hasil.xlsx
 */

package excelpreprocessta;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author User
 */
public class ExcelPreProcessTA {

    public static void printCell(Cell cell){
        switch(cell.getCellType()) {
            case Cell.CELL_TYPE_BOOLEAN:
                System.out.println(cell.getBooleanCellValue());
                break;
            case Cell.CELL_TYPE_NUMERIC:
                System.out.println(cell.getNumericCellValue());
                break;
            case Cell.CELL_TYPE_STRING:
                System.out.println(cell.getStringCellValue());
                break;
            case Cell.CELL_TYPE_BLANK:
                System.out.println("null");
                break;
        }
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        String sumber = "C:\\Users\\User\\Documents\\data weka TA\\mineral.xlsx";
        String hasil = "C:\\Users\\User\\Documents\\data weka TA\\mineral_hasil.xlsx";
        
        try {
            
            int jumlahAtribut = 11; //12-1
            FileInputStream file = new FileInputStream(new File(sumber));

            //Get the workbook instance for XLS file 
            XSSFWorkbook workbook = new XSSFWorkbook(file);

            //Get first sheet from the workbook
            XSSFSheet sheet = workbook.getSheetAt(0);

            //Iterate through each rows from first sheet
//            Iterator<Row> rowIterator = sheet.iterator();
//            while(rowIterator.hasNext()) {
//                Row row = rowIterator.next();

                //For each row, iterate through each columns
//                Iterator<Cell> cellIterator = row.cellIterator();
//                while(cellIterator.hasNext()) {

//                    Cell cell = cellIterator.next();
            Cell cell;           
            int rowAkhirYgTerisi = 0;
            int lastRowInit = sheet.getLastRowNum();        //simpan nomor row terakhir file input
            boolean rowUdahDiProses = false;
            for(int i=0;i<=lastRowInit;i++){            
                for(int j=0;j<=jumlahAtribut;j++){
                    cell = sheet.getRow(i).getCell(j);  
                    System.out.println(i+" "+j);
//                    printCell(cell);      
                    switch(cell.getCellType()) {
                        case Cell.CELL_TYPE_BOOLEAN:
                            System.out.println(cell.getBooleanCellValue());
                            break;
                        case Cell.CELL_TYPE_NUMERIC:
                            cell.setCellValue(cell.getNumericCellValue());
                            break;
                        case Cell.CELL_TYPE_STRING:
                            System.out.println(cell.getStringCellValue());
                            if(!rowUdahDiProses && cell.getStringCellValue().contains(",")){        //proses cell yg ada char ","
                                System.out.println("tes");
                                System.out.println(i+","+j);
                                String str = cell.getStringCellValue();
                                String[] arrayStr = str.split(",");
                                cell.setCellValue(arrayStr[0]);                                
                                for(int ii=1;ii<arrayStr.length;ii++){
                                    rowAkhirYgTerisi++;
                                    XSSFRow row = sheet.createRow(lastRowInit+rowAkhirYgTerisi);
//                                    sheet.shiftRows(0, lastRowInit, 1);
                                    for(int jj=0; jj<sheet.getRow(i).getLastCellNum(); jj++){
                                        Cell selBaru = row.createCell(jj,Cell.CELL_TYPE_STRING);
                                        Cell selAsal = sheet.getRow(i).getCell(jj);
                                        switch(selAsal.getCellType()){
                                            case Cell.CELL_TYPE_BOOLEAN:
                                                selBaru.setCellValue(sheet.getRow(i).getCell(jj).getBooleanCellValue());
                                                break;
                                            case Cell.CELL_TYPE_NUMERIC:
                                                selBaru.setCellValue(sheet.getRow(i).getCell(jj).getNumericCellValue());
                                                break;
                                            case Cell.CELL_TYPE_STRING:
                                                selBaru.setCellValue(sheet.getRow(i).getCell(jj).getStringCellValue());
                                                break;
                                        }//end switch                                        
                                    }
                                    Cell sel = row.createCell(j,Cell.CELL_TYPE_STRING);
                                    sel.setCellValue(arrayStr[ii]);
                                }        
                                rowUdahDiProses = true;
                            }
                            break;
                        case Cell.CELL_TYPE_BLANK:
                            System.out.println("null");
                            break;
                    }
//                    cell.setCellValue(2);
                }//end for j
                rowUdahDiProses = false;
            }//end for i
                         
//                    switch(cell.getCellType()) {
//                        case Cell.CELL_TYPE_BOOLEAN:
//                            System.out.print(cell.getBooleanCellValue() + "\t\t");
//                            break;
//                        case Cell.CELL_TYPE_NUMERIC:
//                            System.out.print(cell.getNumericCellValue() + "\t\t");
//                            break;
//                        case Cell.CELL_TYPE_STRING:
//                            System.out.print(cell.getStringCellValue() + "\t\t");
//                            break;
//                    }
//                }
                System.out.println("");
//            }
            file.close();
            FileOutputStream out = 
                new FileOutputStream(new File(hasil));
            workbook.write(out);
            out.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        
    }
    
}
